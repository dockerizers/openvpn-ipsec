#!/bin/bash

# Set log file paths (modify as needed)
LOG_FILE="/var/log/ipsec_monitor.log"
INFO_LOG_FILE="/var/log/ipsec_monitor_info.log"

IFS=',' read -r -a CONNECTION_ARRAY <<< "$CONNECTIONS"
IFS=',' read -r -a CONNECTION_IP_ARRAY <<< "$CONNECTION_IPS"

if [ ${#CONNECTION_ARRAY[@]} -ne ${#CONNECTION_IP_ARRAY[@]} ]; then
    echo "Error: The number of connections does not match the number of IPs."
    exit 1
fi


# Function to write a message to the main log file
function log_message() {
  message="$1"
  echo "$(date): $message" >> "$LOG_FILE"
}

# Function to write a message to the informational log file (optional)
function log_info() {
  message="$1"
  echo "$(date): $message" >> "$INFO_LOG_FILE" 2>&1
}

# Function to check if the tunnel is up using openswan commands
function is_tunnel_up() {
  # Check if the tunnel is listed and established 
  ipsec whack --status | grep -q "esp.*@$2"
}

# Function to restart the tunnel using openswan commands
function restart_tunnel() {
  # Try to bring the connection up
  ipsec auto --up $1
  # Check the status again after bringing it up
  if ! is_tunnel_up "$1" "$2"; then
    log_message "Failed to restart IPSec tunnel $1"
  else
    log_info "Successfully restarted IPSec tunnel $1"
  fi
}

# Loop to continuously monitor and restart the tunnel if down
while true; do
  # Check if tunnel is up
  for i in "${!CONNECTION_ARRAY[@]}"; do
    CONNECTION="${CONNECTION_ARRAY[$i]}"
    CONNECTION_IP="${CONNECTION_IP_ARRAY[$i]}"

    if is_tunnel_up "$CONNECTION" "$CONNECTION_IP"; then
      log_info "IPSec tunnel $CONNECTION is UP"
    else
      log_message "IPSec tunnel $CONNECTION is DOWN, restarting..."
      restart_tunnel "$CONNECTION" "$CONNECTION_IP"
    fi
  done
  # Wait for some time before checking again (adjust as needed)
  sleep 30
done
